package academico.gui.cadastro.professor;

import academico.controller.ProfessorController;
import academico.model.ProfessorVO;
import academico.tipo.url.TituloProfessor;
import academico.util.Formatacao;
import javax.swing.JOptionPane;

public class ProfessorCadastroUI extends javax.swing.JInternalFrame {

    public ProfessorCadastroUI() {
        initComponents();

        cbbTitulo.addItem(TituloProfessor.ASSISTENTE.getDescricao());
        cbbTitulo.addItem(TituloProfessor.COLABORADOR.getDescricao());
        cbbTitulo.addItem(TituloProfessor.MESTRE.getDescricao());
        cbbTitulo.addItem(TituloProfessor.DOUTOR.getDescricao());

        cbbTitulo.getSelectedItem();
    }

    public void carregar(int i_id) throws Exception {
        ProfessorVO oProfessor = new ProfessorController().carregar(i_id);

        txtId.setText(String.valueOf(oProfessor.id));
        txtNome.setText(oProfessor.nome);
        txtRG.setText(oProfessor.rg);
        txtCpf.setText(oProfessor.cpf);

        switch (oProfessor.idTitulo) {
            case 1:
                cbbTitulo.setSelectedItem(TituloProfessor.ASSISTENTE.getDescricao());
                break;

            case 2:
                cbbTitulo.setSelectedItem(TituloProfessor.COLABORADOR.getDescricao());
                break;

            case 3:
                cbbTitulo.setSelectedItem(TituloProfessor.MESTRE.getDescricao());
                break;

            case 4:
                cbbTitulo.setSelectedItem(TituloProfessor.DOUTOR.getDescricao());
                break;
            default:
                throw new AssertionError();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnIncluir = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        txtRG = new javax.swing.JTextField();
        txtCpf = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbbTitulo = new javax.swing.JComboBox<>();

        setClosable(true);
        setMaximizable(true);
        setTitle("Cadastro de Professor");

        jToolBar1.setFloatable(false);

        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/Novo_2.png"))); // NOI18N
        btnIncluir.setFocusable(false);
        btnIncluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIncluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnIncluir);

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/Salvar_2.png"))); // NOI18N
        btnSalvar.setFocusable(false);
        btnSalvar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalvar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnSalvar);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Nome");

        jLabel2.setText("RG");

        jLabel3.setText("CPF");

        jLabel4.setText("Codigo");

        txtId.setEditable(false);

        txtRG.setColumns(15);

        txtCpf.setColumns(15);

        jLabel5.setText("Título");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtRG, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(186, 186, 186))
                                .addComponent(txtCpf)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtNome)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addGap(0, 0, Short.MAX_VALUE)))))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cbbTitulo, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbbTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        txtId.setText("");
        txtNome.setText("");
        txtRG.setText("");
        txtCpf.setText("");

    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            ProfessorVO oProfessor = new ProfessorVO();

            if (txtNome.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe o nome!");
                return;
            }

            if (txtRG.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe o RG!");
                return;
            }

            if (txtCpf.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe o RG!");
                return;
            }

            oProfessor.id = (txtId.getText().isEmpty() ? 0 : Integer.parseInt(txtId.getText()));
            oProfessor.nome = txtNome.getText();
            oProfessor.rg = txtRG.getText();
            oProfessor.cpf = txtCpf.getText();

            if (cbbTitulo.getSelectedItem().toString().equals(TituloProfessor.ASSISTENTE.getDescricao())) {
                oProfessor.idTitulo = TituloProfessor.ASSISTENTE.getId();

            } else if (cbbTitulo.getSelectedItem().toString().equals(TituloProfessor.COLABORADOR.getDescricao())) {
                oProfessor.idTitulo = TituloProfessor.COLABORADOR.getId();

            } else if (cbbTitulo.getSelectedItem().toString().equals(TituloProfessor.MESTRE.getDescricao())) {
                oProfessor.idTitulo = TituloProfessor.MESTRE.getId();

            } else if (cbbTitulo.getSelectedItem().toString().equals(TituloProfessor.DOUTOR.getDescricao())) {
                oProfessor.idTitulo = TituloProfessor.DOUTOR.getId();
            }

            if (!oProfessor.cpf.isEmpty() && !new Formatacao().cpfValido(oProfessor.cpf)) {
                JOptionPane.showMessageDialog(this, "CPF inválido!");
                return;
            }

            if (!oProfessor.rg.isEmpty() && oProfessor.rg.length() > 15) {
                JOptionPane.showMessageDialog(this, "RG inválido!");
                return;
            }

            new ProfessorController().salvar(oProfessor);

            JOptionPane.showMessageDialog(this, "Registro salvo com cucesso!");

            txtId.setText(String.valueOf(oProfessor.id));

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }

    }//GEN-LAST:event_btnSalvarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cbbTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtCpf;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtRG;
    // End of variables declaration//GEN-END:variables
}
