package academico.gui.cadastro.disciplinas;

import academico.controller.DisciplinaController;
import academico.controller.ProfessorController;
import academico.gui.cadastro.aluno.AlunoConsultaUI;
import academico.gui.cadastro.disciplinas.ementa.EmentaConsultaUI;
import academico.model.AlunoVO;
import academico.model.DisciplinaVO;
import academico.model.EmentaVO;
import academico.model.ProfessorVO;
import academico.tipo.DiaSemana;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class DisciplinaCadastroUI extends javax.swing.JInternalFrame {

    public DisciplinaVO oDisciplina = new DisciplinaVO();

    public DisciplinaCadastroUI() throws Exception {
        initComponents();

        cbbDiaSemana.addItem(DiaSemana.DOMINGO.getDescricao());
        cbbDiaSemana.addItem(DiaSemana.SEGUNDA.getDescricao());
        cbbDiaSemana.addItem(DiaSemana.TERCA.getDescricao());
        cbbDiaSemana.addItem(DiaSemana.QUARTA.getDescricao());
        cbbDiaSemana.addItem(DiaSemana.QUINTA.getDescricao());
        cbbDiaSemana.addItem(DiaSemana.SEXTA.getDescricao());
        cbbDiaSemana.addItem(DiaSemana.SABADO.getDescricao());

        carregarComboProfessor();
    }

    public void carregar(int i_id) throws Exception {
        oDisciplina = new DisciplinaController().carregar(i_id);

        txtId.setText(String.valueOf(oDisciplina.id));
        txtDescricao.setText(oDisciplina.descricao);
        txtVagas.setText(String.valueOf(oDisciplina.vagas));
        txtCargaHoraria.setText(String.valueOf(oDisciplina.cargaHoraria));

        switch (oDisciplina.idDiaSemana) {
            case 1:
                cbbDiaSemana.setSelectedItem(DiaSemana.DOMINGO.getDescricao());
                break;

            case 2:
                cbbDiaSemana.setSelectedItem(DiaSemana.SEGUNDA.getDescricao());
                break;

            case 3:
                cbbDiaSemana.setSelectedItem(DiaSemana.TERCA.getDescricao());
                break;

            case 4:
                cbbDiaSemana.setSelectedItem(DiaSemana.QUARTA.getDescricao());
                break;

            case 5:
                cbbDiaSemana.setSelectedItem(DiaSemana.QUINTA.getDescricao());
                break;

            case 6:
                cbbDiaSemana.setSelectedItem(DiaSemana.SEXTA.getDescricao());
                break;

            case 7:
                cbbDiaSemana.setSelectedItem(DiaSemana.SABADO.getDescricao());
                break;
            default:
                throw new AssertionError();
        }

        selecionarItemCombo();

        exibirEmenta();

        exibirAluno();
    }

    public void carregarComboProfessor() throws Exception {
        ArrayList<ProfessorVO> v_professor = new ArrayList();

        v_professor = new ProfessorController().consultar();

        cbbProfessor.removeAllItems();

        Object[] items = v_professor.toArray();
        DefaultComboBoxModel model = new DefaultComboBoxModel(items);
        cbbProfessor.setModel(model);
    }

    public void selecionarItemCombo() throws Exception {
        int index = -1;

        for (int i = 0; i < cbbProfessor.getItemCount(); i++) {
            String itemCombo = String.valueOf(cbbProfessor.getItemAt(i));
            int idItemCombo = Integer.parseInt(itemCombo.substring(0, itemCombo.lastIndexOf("-")).trim());

            if (idItemCombo == oDisciplina.idProfessor) {
                index = i;
            }
        }

        cbbProfessor.setSelectedIndex(index);
    }

    public int getItemCombo() throws Exception {
        int idItemCombo = -1;
        
            String itemCombo = String.valueOf(  cbbProfessor.getSelectedItem());
            idItemCombo = Integer.parseInt(itemCombo.substring(0, itemCombo.lastIndexOf("-")).trim());

        return idItemCombo;
    }

    public void exibirEmenta() throws Exception {
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new Object[]{"Codigo", "Descrição"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (EmentaVO oEmenta : oDisciplina.v_ementa) {
            model.addRow(new String[]{String.valueOf(oEmenta.id), oEmenta.descricao});
        }

        tblEmenta.setModel(model);
    }

    public void exibirAluno() throws Exception {
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new Object[]{"Matrícula", "Nome"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (AlunoVO oAluno : oDisciplina.v_aluno) {
            model.addRow(new String[]{String.valueOf(oAluno.matricula), oAluno.nome});
        }

        tblAluno.setModel(model);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnIncluir = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtDescricao = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbbDiaSemana = new javax.swing.JComboBox<>();
        txtVagas = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        cbbProfessor = new javax.swing.JComboBox<>();
        txtCargaHoraria = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEmenta = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblAluno = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        setClosable(true);
        setMaximizable(true);
        setTitle("Cadastro de Disciplina");

        jToolBar1.setFloatable(false);

        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/Novo_2.png"))); // NOI18N
        btnIncluir.setFocusable(false);
        btnIncluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIncluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnIncluir);

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/Salvar_2.png"))); // NOI18N
        btnSalvar.setFocusable(false);
        btnSalvar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalvar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnSalvar);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Descrição");

        jLabel2.setText("Vagas");

        jLabel3.setText("Carga Horária");

        jLabel4.setText("Codigo");

        txtId.setEditable(false);

        jLabel5.setText("Dia da Semana");

        txtVagas.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#"))));

        jLabel6.setText("Professor");

        txtCargaHoraria.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#"))));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Ementa"));

        tblEmenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblEmenta);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/RemoverItem.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/AdicionarItem.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Aluno"));

        tblAluno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tblAluno);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/RemoverItem.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/AdicionarItem.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 190, Short.MAX_VALUE)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                            .addComponent(txtVagas))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCargaHoraria, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbbDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(cbbProfessor, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtVagas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCargaHoraria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbbProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        try {
            oDisciplina = new DisciplinaVO();

            txtId.setText("");
            txtDescricao.setText("");
            txtCargaHoraria.setText("");
            txtVagas.setText("");

            exibirEmenta();
            exibirAluno();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }

    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            oDisciplina.id = (txtId.getText().isEmpty() ? 0 : Integer.parseInt(txtId.getText()));
            oDisciplina.descricao = txtDescricao.getText();
            oDisciplina.idProfessor = getItemCombo();
            oDisciplina.vagas = Integer.parseInt(txtVagas.getText());
            oDisciplina.cargaHoraria = Integer.parseInt(txtCargaHoraria.getText());

            if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.DOMINGO.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.DOMINGO.getId();

            } else if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.SEGUNDA.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.SEGUNDA.getId();

            } else if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.TERCA.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.TERCA.getId();

            } else if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.QUARTA.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.QUARTA.getId();

            } else if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.QUINTA.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.QUINTA.getId();

            } else if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.SEXTA.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.SEXTA.getId();

            } else if (cbbDiaSemana.getSelectedItem().toString().equals(DiaSemana.SABADO.getDescricao())) {
                oDisciplina.idDiaSemana = DiaSemana.SABADO.getId();
            }

            if (oDisciplina.cargaHoraria <= 0) {
                JOptionPane.showMessageDialog(this, "A carga horária deve ser maior que 0!");
                return;
            }

            if (oDisciplina.cargaHoraria > 40) {
                JOptionPane.showMessageDialog(this, "A carga horária deve ser menor que 40!");
                return;
            }

            if (new DisciplinaController().diaSemanaDisponivel(oDisciplina.id, oDisciplina.idDiaSemana)) {
                JOptionPane.showMessageDialog(this, "Já existe disciplina cadastrada para este dia da semana!");
                return;
            }

            if (oDisciplina.v_aluno.size() > oDisciplina.vagas) {
                JOptionPane.showMessageDialog(this, "Quantidade de alunos maior que número de vagas!");
                return;
            }
            
            new DisciplinaController().salvar(oDisciplina);
            
            txtId.setText(String.valueOf(oDisciplina.id));

            JOptionPane.showMessageDialog(this, "Registro salvo com cucesso!");

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }

    }//GEN-LAST:event_btnSalvarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        EmentaConsultaUI frmEmentaConsulta = new EmentaConsultaUI(this);
        getParent().add(frmEmentaConsulta);
        frmEmentaConsulta.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            if (tblEmenta.getSelectedRow() > -1) {
                int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir", "Sim ou não?", JOptionPane.YES_NO_OPTION);
                boolean achou = false;

                if (opcao == JOptionPane.YES_OPTION) {
                    EmentaVO oEmentaExclusao = new EmentaVO();
                    int id = Integer.parseInt(tblEmenta.getValueAt(tblEmenta.getSelectedRow(), 0).toString());

                    for (EmentaVO oEmenta : oDisciplina.v_ementa) {
                        if (id == oEmenta.id) {
                            oEmentaExclusao = oEmenta;
                            achou = true;
                        }
                    }

                    if (achou) {
                        oDisciplina.v_ementa.remove(oEmentaExclusao);
                    }

                    JOptionPane.showMessageDialog(this, "Registro excluído com sucesso!");
                    exibirEmenta();
                }

            } else {
                JOptionPane.showMessageDialog(this, "Nenhum registro selecionado!");
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            if (tblAluno.getSelectedRow() > -1) {
                int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir", "Sim ou não?", JOptionPane.YES_NO_OPTION);
                boolean achou = false;

                if (opcao == JOptionPane.YES_OPTION) {
                    AlunoVO oAlunoExclusao = new AlunoVO();
                    String matricula = tblAluno.getValueAt(tblAluno.getSelectedRow(), 0).toString();

                    for (AlunoVO oAluno : oDisciplina.v_aluno) {
                        if (matricula.equals(oAluno.matricula)) {
                            oAlunoExclusao = oAluno;
                            achou = true;
                        }
                    }

                    if (achou) {
                        oDisciplina.v_aluno.remove(oAlunoExclusao);
                    }

                    JOptionPane.showMessageDialog(this, "Registro excluído com sucesso!");
                    exibirAluno();
                }

            } else {
                JOptionPane.showMessageDialog(this, "Nenhum registro selecionado!");
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if (txtVagas.getText().isEmpty() || Integer.parseInt(txtVagas.getText()) <= 0) {
            JOptionPane.showMessageDialog(this, "Informe a quantidade de vagas para inserir um aluno!");

        } else {
            AlunoConsultaUI frmAlunoConsulta = new AlunoConsultaUI(this);
            getParent().add(frmAlunoConsulta);
            frmAlunoConsulta.setVisible(true);

        }
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cbbDiaSemana;
    private javax.swing.JComboBox<String> cbbProfessor;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tblAluno;
    private javax.swing.JTable tblEmenta;
    private javax.swing.JFormattedTextField txtCargaHoraria;
    private javax.swing.JTextField txtDescricao;
    private javax.swing.JTextField txtId;
    private javax.swing.JFormattedTextField txtVagas;
    // End of variables declaration//GEN-END:variables
}
