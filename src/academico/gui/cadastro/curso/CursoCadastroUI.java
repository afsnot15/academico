package academico.gui.cadastro.curso;

import academico.controller.CursoController;
import academico.gui.cadastro.disciplinas.DisciplinaConsultaUI;
import academico.model.CursoVO;
import academico.model.DisciplinaVO;
import academico.tipo.Periodo;
import academico.tipo.TipoDuracao;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class CursoCadastroUI extends javax.swing.JInternalFrame {

    public CursoVO oCurso = new CursoVO();

    public CursoCadastroUI() throws Exception {
        initComponents();

        cbbPeriodo.addItem(Periodo.MATUTINO.getDescricao());
        cbbPeriodo.addItem(Periodo.VESPERTINO.getDescricao());
        cbbPeriodo.addItem(Periodo.NOTURNO.getDescricao());
        cbbPeriodo.addItem(Periodo.INTEGRAL.getDescricao());

        cbbTipoDuracao.addItem(TipoDuracao.DIA.getDescricao());
        cbbTipoDuracao.addItem(TipoDuracao.SEMANA.getDescricao());
        cbbTipoDuracao.addItem(TipoDuracao.MES.getDescricao());
        cbbTipoDuracao.addItem(TipoDuracao.ANO.getDescricao());
    }

    public void carregar(int i_id) throws Exception {
        oCurso = new CursoController().carregar(i_id);

        txtId.setText(String.valueOf(oCurso.id));
        txtDescricao.setText(oCurso.descricao);
        txtDuracao.setText(String.valueOf(oCurso.duracao));
        txtQtdAluno.setText(String.valueOf(oCurso.quantidadeAluno));
        txtCargaHoraria.setText(String.valueOf(oCurso.cargaHoraria));

        switch (oCurso.idPeriodo) {
            case 1:
                cbbPeriodo.setSelectedItem(Periodo.MATUTINO.getDescricao());
                break;

            case 2:
                cbbPeriodo.setSelectedItem(Periodo.VESPERTINO.getDescricao());
                break;

            case 3:
                cbbPeriodo.setSelectedItem(Periodo.NOTURNO.getDescricao());
                break;

            case 4:
                cbbPeriodo.setSelectedItem(Periodo.INTEGRAL.getDescricao());
                break;

            default:
                throw new AssertionError();
        }

        switch (oCurso.idTipoDuracao) {
            case 1:
                cbbTipoDuracao.setSelectedItem(TipoDuracao.DIA.getDescricao());
                break;

            case 2:
                cbbTipoDuracao.setSelectedItem(TipoDuracao.SEMANA.getDescricao());
                break;

            case 3:
                cbbTipoDuracao.setSelectedItem(TipoDuracao.MES.getDescricao());
                break;

            case 4:
                cbbTipoDuracao.setSelectedItem(TipoDuracao.ANO.getDescricao());
                break;

            default:
                throw new AssertionError();

        }

        exibirDisciplina();
    }

    public void exibirDisciplina() throws Exception {
        int quantidadeAluno = 0;
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, new Object[]{"Codigo", "Descricao", "Carga Horária"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (DisciplinaVO oDisciplina : oCurso.v_disciplina) {
            model.addRow(new String[]{String.valueOf(oDisciplina.id), oDisciplina.descricao, String.valueOf(oDisciplina.cargaHoraria)});
            quantidadeAluno += oDisciplina.v_aluno.size();
        }

        txtQtdAluno.setText(String.valueOf(quantidadeAluno));

        tblDisciplina.setModel(model);
    }

    public int getCargaHorariaTotal() {
        int totalcargaHoraria = 0;

        for (DisciplinaVO oDisciplina : oCurso.v_disciplina) {
            totalcargaHoraria += oDisciplina.cargaHoraria;
        }

        return totalcargaHoraria;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        btnIncluir = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtDescricao = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbbPeriodo = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtQtdAluno = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDisciplina = new javax.swing.JTable();
        btnRemoverDisciplina = new javax.swing.JButton();
        btnAdicionarDisciplina = new javax.swing.JButton();
        cbbTipoDuracao = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtCargaHoraria = new javax.swing.JFormattedTextField();
        txtDuracao = new javax.swing.JFormattedTextField();

        setClosable(true);
        setMaximizable(true);
        setTitle("Cadastro de Curso");

        jToolBar1.setFloatable(false);

        btnIncluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/Novo_2.png"))); // NOI18N
        btnIncluir.setFocusable(false);
        btnIncluir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIncluir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncluirActionPerformed(evt);
            }
        });
        jToolBar1.add(btnIncluir);

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/Salvar_2.png"))); // NOI18N
        btnSalvar.setFocusable(false);
        btnSalvar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSalvar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });
        jToolBar1.add(btnSalvar);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Descrição");

        jLabel2.setText("Duração");

        jLabel3.setText("Período");

        jLabel4.setText("Codigo");

        txtId.setEditable(false);

        jLabel5.setText("Quantidade Alunos");

        jLabel6.setText("Carga Horaria");

        txtQtdAluno.setEditable(false);
        txtQtdAluno.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#"))));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Disciplina"));

        tblDisciplina.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblDisciplina);

        btnRemoverDisciplina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/RemoverItem.png"))); // NOI18N
        btnRemoverDisciplina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoverDisciplinaActionPerformed(evt);
            }
        });

        btnAdicionarDisciplina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/academico/img/AdicionarItem.png"))); // NOI18N
        btnAdicionarDisciplina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarDisciplinaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnAdicionarDisciplina)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoverDisciplina))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRemoverDisciplina, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAdicionarDisciplina, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        jLabel7.setText("Tipo Duração");

        txtCargaHoraria.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#"))));

        txtDuracao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("0.0"))));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(txtDuracao)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(cbbTipoDuracao, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(cbbPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                                        .addComponent(txtQtdAluno)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(txtDescricao))))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                                .addComponent(txtCargaHoraria, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbbPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtQtdAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbTipoDuracao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDuracao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCargaHoraria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIncluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncluirActionPerformed
        try {
            oCurso = new CursoVO();

            txtId.setText("");
            txtDescricao.setText("");
            txtQtdAluno.setText("");
            txtDuracao.setText("");
            txtCargaHoraria.setText("");

            exibirDisciplina();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }

    }//GEN-LAST:event_btnIncluirActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            if (txtDescricao.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe a descricao!");
                return;
            }

            if (txtDuracao.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe a duração!");
                return;
            }

            if (txtCargaHoraria.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe a carga horária!");
                return;
            }

            if (oCurso.v_disciplina.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Informe ao menos uma disciplina!");
                return;
            }

            oCurso.id = (txtId.getText().isEmpty() ? 0 : Integer.parseInt(txtId.getText()));
            oCurso.descricao = txtDescricao.getText();
            oCurso.cargaHoraria = Integer.parseInt(txtCargaHoraria.getText());
            oCurso.quantidadeAluno = Integer.parseInt(txtQtdAluno.getText());
            oCurso.duracao = Double.parseDouble(txtDuracao.getText().replace(",", "."));

            if (cbbPeriodo.getSelectedItem().toString().equals(Periodo.MATUTINO.getDescricao())) {
                oCurso.idPeriodo = Periodo.MATUTINO.getId();

            } else if (cbbPeriodo.getSelectedItem().toString().equals(Periodo.VESPERTINO.getDescricao())) {
                oCurso.idPeriodo = Periodo.VESPERTINO.getId();

            } else if (cbbPeriodo.getSelectedItem().toString().equals(Periodo.NOTURNO.getDescricao())) {
                oCurso.idPeriodo = Periodo.NOTURNO.getId();

            } else if (cbbPeriodo.getSelectedItem().toString().equals(Periodo.INTEGRAL.getDescricao())) {
                oCurso.idPeriodo = Periodo.INTEGRAL.getId();
            }

            if (oCurso.cargaHoraria < 20 || oCurso.cargaHoraria > 40) {
                JOptionPane.showMessageDialog(this, "A carga horária deve ser entre 20 e 40 horas!");
                return;
            }

            if (cbbTipoDuracao.getSelectedItem().toString().equals(TipoDuracao.DIA.getDescricao())) {
                oCurso.idTipoDuracao = TipoDuracao.DIA.getId();

            } else if (cbbTipoDuracao.getSelectedItem().toString().equals(TipoDuracao.SEMANA.getDescricao())) {
                oCurso.idTipoDuracao = TipoDuracao.SEMANA.getId();

            } else if (cbbTipoDuracao.getSelectedItem().toString().equals(TipoDuracao.MES.getDescricao())) {
                oCurso.idTipoDuracao = TipoDuracao.MES.getId();

            } else if (cbbTipoDuracao.getSelectedItem().toString().equals(TipoDuracao.ANO.getDescricao())) {
                oCurso.idTipoDuracao = TipoDuracao.ANO.getId();
            }

            if (Integer.parseInt(txtCargaHoraria.getText()) < getCargaHorariaTotal()) {
                JOptionPane.showMessageDialog(this, "A carga horária das disciplinas ultrapassa o limite do curso!");
                return;
            }

            //Verificar Professor
            if (new CursoController().professorMestreDoutor(oCurso.v_disciplina)) {
                JOptionPane.showMessageDialog(this, "Existem mais de um professor com o título de mestre ou doutor!");
                return;
            }

            new CursoController().salvar(oCurso);

            txtId.setText(String.valueOf(oCurso.id));

            JOptionPane.showMessageDialog(this, "Registro salvo com cucesso!");

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnAdicionarDisciplinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarDisciplinaActionPerformed
        DisciplinaConsultaUI frmDisciplinaConsulta = new DisciplinaConsultaUI(this);
        getParent().add(frmDisciplinaConsulta);
        frmDisciplinaConsulta.setVisible(true);
    }//GEN-LAST:event_btnAdicionarDisciplinaActionPerformed

    private void btnRemoverDisciplinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoverDisciplinaActionPerformed
        try {
            if (tblDisciplina.getSelectedRow() > -1) {
                int opcao = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir", "Sim ou não?", JOptionPane.YES_NO_OPTION);
                boolean achou = false;

                if (opcao == JOptionPane.YES_OPTION) {
                    DisciplinaVO oDisciplinaExclusao = new DisciplinaVO();
                    int id = Integer.parseInt(tblDisciplina.getValueAt(tblDisciplina.getSelectedRow(), 0).toString());

                    for (DisciplinaVO oDisciplina : oCurso.v_disciplina) {
                        if (id == oDisciplina.id) {
                            oDisciplinaExclusao = oDisciplina;
                            achou = true;
                        }
                    }

                    if (achou) {
                        oCurso.v_disciplina.remove(oDisciplinaExclusao);
                    }

                    JOptionPane.showMessageDialog(this, "Registro excluído com sucesso!");
                    exibirDisciplina();
                }

            } else {
                JOptionPane.showMessageDialog(this, "Nenhum registro selecionado!");
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
    }//GEN-LAST:event_btnRemoverDisciplinaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionarDisciplina;
    private javax.swing.JButton btnIncluir;
    private javax.swing.JButton btnRemoverDisciplina;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> cbbPeriodo;
    private javax.swing.JComboBox<String> cbbTipoDuracao;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTable tblDisciplina;
    private javax.swing.JFormattedTextField txtCargaHoraria;
    private javax.swing.JTextField txtDescricao;
    private javax.swing.JFormattedTextField txtDuracao;
    private javax.swing.JTextField txtId;
    private javax.swing.JFormattedTextField txtQtdAluno;
    // End of variables declaration//GEN-END:variables
}
