package academico.gui;

import academico.gui.cadastro.aluno.AlunoConsultaUI;
import academico.gui.cadastro.consulta.CursosConsultaUI;
import academico.gui.cadastro.curso.CursoConsultaUI;
import academico.gui.cadastro.disciplinas.DisciplinaConsultaUI;
import academico.gui.cadastro.disciplinas.ementa.EmentaConsultaUI;
import academico.gui.cadastro.professor.ProfessorConsultaUI;

public class MenuUI extends javax.swing.JFrame {

    public MenuUI() {
        initComponents();

        this.setExtendedState(MenuUI.MAXIMIZED_BOTH);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        mnuAluno = new javax.swing.JMenuItem();
        mnuEmenta = new javax.swing.JMenuItem();
        mnuProfessor = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Academico");

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 628, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 429, Short.MAX_VALUE)
        );

        jMenu1.setText("Cadastro");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Curso");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        mnuAluno.setText("Aluno");
        mnuAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuAlunoActionPerformed(evt);
            }
        });
        jMenu1.add(mnuAluno);

        mnuEmenta.setText("Ementa");
        mnuEmenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuEmentaActionPerformed(evt);
            }
        });
        jMenu1.add(mnuEmenta);

        mnuProfessor.setText("Professor");
        mnuProfessor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuProfessorActionPerformed(evt);
            }
        });
        jMenu1.add(mnuProfessor);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Consultas");

        jMenuItem2.setText("Cursos");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void mnuProfessorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuProfessorActionPerformed
        ProfessorConsultaUI frmProfessor = new ProfessorConsultaUI();
        frmProfessor.setVisible(true);
        jDesktopPane1.add(frmProfessor);
    }//GEN-LAST:event_mnuProfessorActionPerformed

    private void mnuEmentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuEmentaActionPerformed
        EmentaConsultaUI frmEmenta = new EmentaConsultaUI();
        frmEmenta.setVisible(true);
        jDesktopPane1.add(frmEmenta);
    }//GEN-LAST:event_mnuEmentaActionPerformed

    private void mnuAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuAlunoActionPerformed
        AlunoConsultaUI frmAluno = new AlunoConsultaUI();
        frmAluno.setVisible(true);
        jDesktopPane1.add(frmAluno);
    }//GEN-LAST:event_mnuAlunoActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        CursoConsultaUI frmCursoConsulta = new CursoConsultaUI();
        frmCursoConsulta.setVisible(true);
        jDesktopPane1.add(frmCursoConsulta);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        CursosConsultaUI frmCursoConsulta = new CursosConsultaUI();
        frmCursoConsulta.setVisible(true);
        jDesktopPane1.add(frmCursoConsulta);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem mnuAluno;
    private javax.swing.JMenuItem mnuEmenta;
    private javax.swing.JMenuItem mnuProfessor;
    // End of variables declaration//GEN-END:variables
}
