package academico;

import academico.gui.MenuUI;
import javax.swing.UIManager;

public class Main {

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        MenuUI formLogin = new MenuUI();
        formLogin.setVisible(true);
    }
}
