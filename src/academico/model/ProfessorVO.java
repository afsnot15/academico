package academico.model;

public class ProfessorVO {

    public int id = 0;
    public String nome = "";
    public String rg = "";
    public String cpf = "";
    public int idTitulo = 0;
    public String titulo = "";

    public ProfessorVO() {

    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return id + " - " + nome;
    }
}
