package academico.model;

import java.util.ArrayList;

public class DisciplinaVO {

    public int id = 0;
    public String descricao = "";
    public int vagas = 0;
    public int idDiaSemana = 0;
    public String diaSemana = "";
    public String professor = "";
    public int idProfessor = 0;
    public int cargaHoraria = 0;
    public ArrayList<EmentaVO> v_ementa = new ArrayList();
    public ArrayList<AlunoVO> v_aluno = new ArrayList();

}
