package academico.model;

import java.util.ArrayList;


public class CursoVO {
    
    public int id = 0;
    public String descricao = "";
    public String periodo = "";
    public String tipoduracao = "";
    public double duracao = 0;
    public int idPeriodo = 0;
    public int idTipoDuracao = 0;
    public int quantidadeAluno = 0;
    public int cargaHoraria = 0;
    public ArrayList<DisciplinaVO> v_disciplina = new ArrayList();
    
}
