package academico.util;

import java.text.DecimalFormat;

public class Formatacao {

    public static boolean cpfValido(String i_cpf) throws Exception {
       boolean cpfValido = false;
        String cpf = new DecimalFormat("00000000000").format(Double.parseDouble(i_cpf.replace(".", "").replace("-", "")));

        if (cpf.length() > 11) {
            return false;
        }

        if (cpf.equals("00000000000") || cpf.equals("11111111111")
                || cpf.equals("22222222222") || cpf.equals("33333333333")
                || cpf.equals("44444444444") || cpf.equals("55555555555")
                || cpf.equals("66666666666") || cpf.equals("77777777777")
                || cpf.equals("88888888888") || cpf.equals("99999999999")) {
            cpfValido = false;
        }

        int digitoVerificador = Integer.parseInt(cpf.substring(9, 11));

        int digitoAuxiliar = 0;

        int soma = 0;
        int somaAuxiliar = 0;

        int valor = 10;

        for (int i = 0; i < 9; i++) {
            soma += Integer.parseInt(cpf.substring(i, i + 1)) * valor;

            valor--;
        }

        valor = 11;

        for (int i = 0; i < 10; i++) {
            somaAuxiliar += Integer.parseInt(cpf.substring(i, i + 1)) * valor;

            valor--;
        }

        soma = (soma * 10) % 11;

        if (soma == 10) {
            soma = 0;
        }

        somaAuxiliar = (somaAuxiliar * 10) % 11;

        if (somaAuxiliar == 10) {
            somaAuxiliar = 0;
        }

        digitoAuxiliar = (soma * 10) + somaAuxiliar;
        
        if (digitoAuxiliar == digitoVerificador){
            cpfValido = true;
        }

        return cpfValido;
    }
}
