package academico.tipo;

public enum DiaSemana {
    DOMINGO(1, "DOMINGO"),
    SEGUNDA(2, "SEGUNDA FEIRA"),
    TERCA(3, "TERCA FEIRA"),
    QUARTA(4, "QUARTA FEIRA"),
    QUINTA(5, "QUINTA FEIRA"),
    SEXTA(6, "SEXTA FEIRA"),
    SABADO(7, "SABADO");

    private final int id;
    private final String descricao;

    private DiaSemana(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

}
