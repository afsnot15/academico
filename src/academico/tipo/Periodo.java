package academico.tipo;

public enum Periodo {
    MATUTINO(1, "MATUTINO"),
    VESPERTINO(2, "VESPERTINO"),
    NOTURNO(3, "NOTURNO"),
    INTEGRAL(4, "INTEGRAL");

    private final int id;
    private final String descricao;

    private Periodo(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
}
