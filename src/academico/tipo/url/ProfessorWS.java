package academico.tipo.url;

public enum ProfessorWS {
    CONSULTAR("http://localhost:8080/WebService/webresources/professor/consultar/?"),
    CARREGAR("http://localhost:8080/WebService/webresources/professor/carregar/"),
    SALVAR("http://localhost:8080/WebService/webresources/professor?"),
    EXCLUIR("http://localhost:8080/WebService/webresources/professor/excluir/?"),
    DEPENDENCIA("http://localhost:8080/WebService/webresources/professor/dependencia/");

    private final String descricao;

    private ProfessorWS(String i_descricao) {
        this.descricao = i_descricao;
    }

    public String getUrl() {
        return descricao;
    }
}
