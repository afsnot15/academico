package academico.tipo.url;

public enum AlunoWS {
    CONSULTAR("http://localhost:8080/WebService/webresources/aluno/consultar/?"),
    CARREGAR("http://localhost:8080/WebService/webresources/aluno/carregar/"),
    SALVAR("http://localhost:8080/WebService/webresources/aluno?"),
    EXCLUIR("http://localhost:8080/WebService/webresources/aluno/excluir/?"),
    DEPENDENCIA("http://localhost:8080/WebService/webresources/aluno/dependencia/");

    private final String descricao;

    private AlunoWS(String i_descricao) {
        this.descricao = i_descricao;
    }

    public String getUrl() {
        return descricao;
    }
}
