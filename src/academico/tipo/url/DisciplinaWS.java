package academico.tipo.url;

public enum DisciplinaWS {
    CONSULTAR("http://localhost:8080/WebService/webresources/disciplina/consultar/?"),
    CARREGAR("http://localhost:8080/WebService/webresources/disciplina/carregar/"),
    SALVAR("http://localhost:8080/WebService/webresources/disciplina?"),
    EXCLUIR("http://localhost:8080/WebService/webresources/disciplina/excluir/?"),
    DIASEMANA("http://localhost:8080/WebService/webresources/disciplina/diasemanaocupado/"),
    PROFESSORDISPONIVEL("http://localhost:8080/WebService/webresources/disciplina/isprofessordisponivel/"),
    DEPENDENCIA("http://localhost:8080/WebService/webresources/disciplina/dependencia/");

    private final String descricao;

    private DisciplinaWS(String i_descricao) {
        this.descricao = i_descricao;
    }

    public String getUrl() {
        return descricao;
    }
}
