package academico.tipo.url;

public enum ConsultaCursoWS {
    CONSULTAR("http://localhost:8080/WebService/webresources/consultacurso/cursodisciplina/");

    private final String descricao;

    private ConsultaCursoWS(String i_descricao) {
        this.descricao = i_descricao;
    }

    public String getUrl() {
        return descricao;
    }
}
