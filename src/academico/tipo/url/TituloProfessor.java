package academico.tipo.url;

public enum TituloProfessor {
    ASSISTENTE(1, "ASSISTENTE"),
    COLABORADOR(2, "COLABORADOR"),
    MESTRE(3, "MESTRE"),
    DOUTOR(4, "DOUTOR");

    private final int id;
    private final String descricao;

    private TituloProfessor(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
