package academico.tipo.url;

public enum EmentaWS {
    CONSULTAR("http://localhost:8080/WebService/webresources/ementa/consultar/?"),
    CARREGAR("http://localhost:8080/WebService/webresources/ementa/carregar/"),
    SALVAR("http://localhost:8080/WebService/webresources/ementa?"),
    EXCLUIR("http://localhost:8080/WebService/webresources/ementa/excluir/?"),
    DEPENDENCIA("http://localhost:8080/WebService/webresources/ementa/dependencia/");

    private final String descricao;

    private EmentaWS(String i_descricao) {
        this.descricao = i_descricao;
    }

    public String getUrl() {
        return descricao;
    }
}
