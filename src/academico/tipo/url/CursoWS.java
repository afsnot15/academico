package academico.tipo.url;

public enum CursoWS {
    CONSULTAR("http://localhost:8080/WebService/webresources/curso/consultar/?"),
    CARREGAR("http://localhost:8080/WebService/webresources/curso/carregar/"),
    SALVAR("http://localhost:8080/WebService/webresources/curso?"),
    EXCLUIR("http://localhost:8080/WebService/webresources/curso/excluir/?"),
    PROFESSORMESTREDOUTOR("http://localhost:8080/WebService/webresources/curso/professormestredoutor/");

    private final String descricao;

    private CursoWS(String i_descricao) {
        this.descricao = i_descricao;
    }

    public String getUrl() {
        return descricao;
    }
}
