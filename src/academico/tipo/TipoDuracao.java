package academico.tipo;

public enum TipoDuracao {
    DIA(1, "DIA"),
    SEMANA(2, "SEMANA"),
    MES(3, "MES"),
    ANO(4, "ANO");

    private final int id;
    private final String descricao;

    private TipoDuracao(int i_id, String i_descricao) {
        this.id = i_id;
        this.descricao = i_descricao;
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }
}
