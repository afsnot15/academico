package academico.controller;

import academico.model.DisciplinaVO;
import academico.tipo.url.DisciplinaWS;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class DisciplinaController {

    public ArrayList<DisciplinaVO> consultar(String i_descricao) throws Exception {
        ArrayList<DisciplinaVO> v_disciplina = new ArrayList();
        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(DisciplinaWS.CONSULTAR.getUrl());
        url.append("descricao=");
        url.append(i_descricao);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        DisciplinaVO[] listaDisciplina = json.fromJson(response.toString(), DisciplinaVO[].class);

        for (DisciplinaVO oDisciplina : listaDisciplina) {
            v_disciplina.add(oDisciplina);
        }

        return v_disciplina;
    }

    public DisciplinaVO carregar(int i_id) throws Exception {
        DisciplinaVO oDisciplina = new DisciplinaVO();

        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(DisciplinaWS.CARREGAR.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        oDisciplina = json.fromJson(response.toString(), DisciplinaVO.class);

        return oDisciplina;
    }

    public void salvar(DisciplinaVO i_diDisciplinaVO) throws Exception {
        try {
            String url = DisciplinaWS.SALVAR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            Gson json = new Gson();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json.toJson(i_diDisciplinaVO));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            i_diDisciplinaVO.id = Integer.parseInt(response.toString());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public void excluir(int i_id) throws Exception {
        try {
            String url = DisciplinaWS.EXCLUIR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(String.valueOf(i_id));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean diaSemanaDisponivel(int i_id, int i_idDiaSemana) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(DisciplinaWS.DIASEMANA.getUrl());
        url.append(i_id);
        url.append("/");
        url.append(i_idDiaSemana);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine = "";
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return Boolean.parseBoolean(response.toString());
    }

    public boolean possuiDependencia(int i_id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(DisciplinaWS.DEPENDENCIA.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return Boolean.parseBoolean(response.toString());
    }
}
