package academico.controller;

import academico.model.AlunoVO;
import academico.tipo.url.AlunoWS;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AlunoController {

    public ArrayList<AlunoVO> consultar(String i_nome) throws Exception {
        ArrayList<AlunoVO> v_aluno = new ArrayList();
        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(AlunoWS.CONSULTAR.getUrl());
        url.append("nome=");
        url.append(i_nome);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        AlunoVO[] listaAluno = json.fromJson(response.toString(), AlunoVO[].class);

        for (AlunoVO oAluno : listaAluno) {
            v_aluno.add(oAluno);
        }

        return v_aluno;
    }

    public AlunoVO carregar(int i_id) throws Exception {
        AlunoVO oAluno = new AlunoVO();

        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(AlunoWS.CARREGAR.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        oAluno = json.fromJson(response.toString(), AlunoVO.class);

        return oAluno;
    }

    public void salvar(AlunoVO i_aluno) throws Exception {
        try {
            String url = AlunoWS.SALVAR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            Gson json = new Gson();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json.toJson(i_aluno));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            i_aluno.id = Integer.parseInt(response.toString());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public void excluir(int i_id) throws Exception {
        try {
            String url = AlunoWS.EXCLUIR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(String.valueOf(i_id));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

        } catch (Exception ex) {
            throw ex;
        }
    }

    public boolean possuiDependencia(int i_id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(AlunoWS.DEPENDENCIA.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return Boolean.parseBoolean(response.toString());
    }
}
