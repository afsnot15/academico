package academico.controller;

import academico.model.ConsultaCursoDisciplinaVO;
import academico.tipo.url.ConsultaCursoWS;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ConsultaCursodisciplinaController {

    public ArrayList<ConsultaCursoDisciplinaVO> consultar() throws Exception {
        ArrayList<ConsultaCursoDisciplinaVO> v_consultaCurso = new ArrayList();
        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(ConsultaCursoWS.CONSULTAR.getUrl());

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        ConsultaCursoDisciplinaVO[] listaConsulta = json.fromJson(response.toString(), ConsultaCursoDisciplinaVO[].class);

        for (ConsultaCursoDisciplinaVO oConsultaCurso : listaConsulta) {
            v_consultaCurso.add(oConsultaCurso);
        }

        return v_consultaCurso;
    }

}
