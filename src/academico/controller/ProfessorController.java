package academico.controller;

import academico.model.ProfessorVO;
import academico.tipo.url.ProfessorWS;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ProfessorController {

    public ArrayList<ProfessorVO> consultar() throws Exception {
        return consultar("");
    }

    public ArrayList<ProfessorVO> consultar(String i_nome) throws Exception {
        ArrayList<ProfessorVO> v_professor = new ArrayList();
        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(ProfessorWS.CONSULTAR.getUrl());
        url.append("nome=");
        url.append(i_nome);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        ProfessorVO[] listaPrfessor = json.fromJson(response.toString(), ProfessorVO[].class);

        for (ProfessorVO oProfessor : listaPrfessor) {
            v_professor.add(oProfessor);
        }

        return v_professor;
    }

    public ProfessorVO carregar(int i_id) throws Exception {
        ProfessorVO oProfessor = new ProfessorVO();

        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(ProfessorWS.CARREGAR.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        oProfessor = json.fromJson(response.toString(), ProfessorVO.class);

        return oProfessor;
    }

    public void salvar(ProfessorVO i_professor) throws Exception {
        try {
            String url = ProfessorWS.SALVAR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            Gson json = new Gson();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json.toJson(i_professor));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            i_professor.id = Integer.parseInt(response.toString());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public void excluir(int i_id) throws Exception {
        try {
            String url = ProfessorWS.EXCLUIR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(String.valueOf(i_id));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

        } catch (Exception ex) {
            throw ex;
        }
    }

  public boolean possuiDependencia(int i_id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(ProfessorWS.DEPENDENCIA.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return Boolean.parseBoolean(response.toString());
    }
}
