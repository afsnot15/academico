package academico.controller;

import academico.model.CursoVO;
import academico.model.DisciplinaVO;
import academico.model.ProfessorVO;
import academico.tipo.url.CursoWS;
import academico.tipo.url.DisciplinaWS;
import academico.tipo.url.TituloProfessor;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class CursoController {

    public ArrayList<CursoVO> consultar(String i_descricao) throws Exception {
        ArrayList<CursoVO> v_curso = new ArrayList();
        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(CursoWS.CONSULTAR.getUrl());
        url.append("descricao=");
        url.append(i_descricao);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        CursoVO[] listaCurso = json.fromJson(response.toString(), CursoVO[].class);

        for (CursoVO oCurso : listaCurso) {
            v_curso.add(oCurso);
        }

        return v_curso;
    }

    public CursoVO carregar(int i_id) throws Exception {
        CursoVO oCursoVO = new CursoVO();

        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(CursoWS.CARREGAR.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        oCursoVO = json.fromJson(response.toString(), CursoVO.class);

        return oCursoVO;
    }

    public boolean professorMestreDoutor(ArrayList<DisciplinaVO> i_disciplina) throws Exception {
        int ocorrencia = 0;
        int idprofessor = 0;

        for (DisciplinaVO disciplinaVO : i_disciplina) {
            ProfessorVO oProfessor = new ProfessorController().carregar(disciplinaVO.idProfessor);
            if (oProfessor.idTitulo == TituloProfessor.MESTRE.getId()
                    || oProfessor.idTitulo == TituloProfessor.DOUTOR.getId()) {

                if (idprofessor != oProfessor.id) {
                    ocorrencia++;
                    idprofessor = oProfessor.id;
                }
            }
        }

        if (ocorrencia > 1) {
            return true;

        } else {
            return false;
        }
    }

    public void salvar(CursoVO i_curso) throws Exception {
        try {
            String url = CursoWS.SALVAR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            Gson json = new Gson();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json.toJson(i_curso));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            i_curso.id = Integer.parseInt(response.toString());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public void excluir(int i_id) throws Exception {
        try {
            String url = CursoWS.EXCLUIR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(String.valueOf(i_id));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

        } catch (Exception ex) {
            throw ex;
        }
    }
}
