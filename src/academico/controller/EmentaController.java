package academico.controller;

import academico.model.EmentaVO;
import academico.tipo.url.EmentaWS;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class EmentaController {

    public ArrayList<EmentaVO> consultar(String i_nome) throws Exception {
        ArrayList<EmentaVO> v_ementa = new ArrayList();
        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(EmentaWS.CONSULTAR.getUrl());
        url.append("descricao=");
        url.append(i_nome);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        EmentaVO[] listaEmenta = json.fromJson(response.toString(), EmentaVO[].class);

        for (EmentaVO oEmenta : listaEmenta) {
            v_ementa.add(oEmenta);
        }

        return v_ementa;
    }

    public EmentaVO carregar(int i_id) throws Exception {
        EmentaVO oEmenta = new EmentaVO();

        Gson json = new Gson();

        StringBuilder url = new StringBuilder();
        url.append(EmentaWS.CARREGAR.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        oEmenta = json.fromJson(response.toString(), EmentaVO.class);

        return oEmenta;
    }

    public void salvar(EmentaVO i_ementa) throws Exception {
        try {
            String url = EmentaWS.SALVAR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            Gson json = new Gson();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(json.toJson(i_ementa));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

            i_ementa.id = Integer.parseInt(response.toString());

        } catch (Exception ex) {
            throw ex;
        }
    }

    public void excluir(int i_id) throws Exception {
        try {
            String url = EmentaWS.EXCLUIR.getUrl();
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("PUT");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(String.valueOf(i_id));
            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();

        } catch (Exception ex) {
            throw ex;
        }
    }
    
     public boolean possuiDependencia(int i_id) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(EmentaWS.DEPENDENCIA.getUrl());
        url.append(i_id);

        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return Boolean.parseBoolean(response.toString());
    }
}
