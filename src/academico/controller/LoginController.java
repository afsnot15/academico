package academico.controller;

import academico.tipo.url.AlunoWS;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginController {

    public void login() throws Exception {
        URL obj = new URL(AlunoWS.CONSULTAR.getUrl());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        Gson json = new Gson();
        //String parametros = json.toJson(i_usuario);

        con.setRequestMethod("PUT");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
       // wr.writeBytes(parametros);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

      //  i_usuario = json.fromJson(response.toString(), UsuarioVO.class);

        in.close();
    }
}
